#!/bin/bash
cd /root/
apt install certbot -y
##################
read -p 'Your email address: ' Email
read -p 'Your domain name: ' Domain
echo Your email address is $Email and your domain name is $Domain.
sleep 5s
##################
certbot certonly --expand --agree-tos --text --non-interactive \
  --standalone \
  --email "$Email" \
  -d "$Domain"
sleep 5s
   cat << EOF > /root/letsencrypt.sh
#!/bin/bash
certbot renew --no-self-upgrade 
mv /etc/pve/local/pve-ssl.pem /root/
mv /etc/pve/local/pve-ssl.key /root/
mv /etc/pve/pve-root-ca.pem /root/
cp /etc/letsencrypt/live/"$Domain"/fullchain.pem /etc/pve/local/pve-ssl.pem  
cp /etc/letsencrypt/live/"$Domain"/privkey.pem /etc/pve/local/pve-ssl.key  
cp /etc/letsencrypt/live/"$Domain"/chain.pem /etc/pve/pve-root-ca.pem  
service pveproxy restart  
service pvedaemon restart  
EOF
sleep 5s
chmod +x /root/letsencrypt.sh

echo '0 0 * * * root /root/letsencrypt.sh' >> /etc/crontab
sleep 5s
./letsencrypt.sh
