Automation script create free Let's Encrypt for your Proxmox server and automatically renew them.
1. Download file
wget https://gitlab.com/proxmox15/pve-with-letsencrypt-ssl/-/archive/main/pve-with-letsencrypt-ssl-main.tar.gz
2. Extract files
tar -xvf pve-with-letsencrypt-ssl-main.tar.gz
3. Make it executable file
chmod +x ssl.sh
4. Run automation script
./ssl.sh
